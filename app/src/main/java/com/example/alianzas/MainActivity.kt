package com.example.alianzas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class MainActivity : AppCompatActivity() {

    private val manager = supportFragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showFragment()
    }
    //Inflate the fragment
    private fun showFragment() {
        val transaction = manager.beginTransaction()
        val fragment = CarnetFragment()
        transaction.replace(R.id.fragmet_container, fragment)
        transaction.commit()
    }
}

package com.example.alianzas

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Camera
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.fragment.app.DialogFragment
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.fragment_carnet.*
/*** A simple [Fragment] subclass. */
class CarnetFragment : Fragment(), Question.NoticeDialogListener {

    //Declarate of FireBase
    private lateinit var database: DatabaseReference
    lateinit var storage: FirebaseStorage
    //End declarate of FireBase
    private lateinit var camera: ImageView
    private lateinit var galeria: ImageView
    private lateinit var imageView: ImageView
    private lateinit var rating: Button
    private var imageUri: Uri? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate fragment
        val view = inflater.inflate(R.layout.fragment_carnet, container, false)
        //Show dialogs
        camera = view.findViewById(R.id.logo_carnet)
        galeria = view.findViewById(R.id.logo_carnet)
        imageView = view.findViewById(R.id.logo_carnet)
        rating = view.findViewById(R.id.button_rating)
        //FireBase
        database = FirebaseDatabase.getInstance().reference
        storage = FirebaseStorage.getInstance()
        //End FireBase
        //Show's QuestionDIalog
        imageView.setOnClickListener {
            showQuestion()
        }

        camera.setOnClickListener {
            showQuestion()
        }

        galeria.setOnClickListener {
            showQuestion()
        }
        //Show RatingDialog
        rating.setOnClickListener {
            showRating()
        }

        return view
    }

   //Permission open camera
    private fun permissionOpenCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity!!.checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                activity!!.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                //permission was not enabled
                Log.d("PERMISSION_STATUS", "NOT GRANTED")
                val permission = arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                //show popup to request permission
                requestPermissions(permission, PERMISSION_CODE_CAMERA)
            } else {
                //permission already granted
                Log.d("PERMISSION_STATUS", "YES GRANTED")
                openCamera()
            }
        } else {
            //system os < Marshmallow
            openCamera()
        }
    }

    //Permission camera
    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        imageUri = activity?.contentResolver?.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            activity?.packageManager?.let {
                takePictureIntent.resolveActivity(it)?.also {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
    }

    //Permission gallery
    private fun permissionPickImageFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity!!.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                //permission denied
                val permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                requestPermissions(permissions, PERMISSION_CODE_GALLERY)
            } else {
                //permission already granted
                pickImageFromGallery()
            }
        } else {
            //System OS is < Marshmallow
            pickImageFromGallery()
        }
    }

    //Image from Gallery
    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    //Permissions
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //called when user presses ALLOW or DENY from permission request popup
        when (requestCode){
            PERMISSION_CODE_CAMERA -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permission from popup was granted
                    openCamera()
                } else {
                    //Permission from popup was denied
                    Toast.makeText(context, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
            PERMISSION_CODE_GALLERY -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permission from popup granted
                    pickImageFromGallery()
                } else {
                    //Permission from popup denied
                    Toast.makeText(context, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //Called when image was captured from the camera intent
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_IMAGE_CAPTURE) {
            //set image captured to image view
            imageView.setImageURI(imageUri)
        } else if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            imageView.setImageURI(data?.data)
        }
    }

    //Open camera and gallery
    override fun onDialogClick(postion: Int) {

        if (postion == 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (activity!!.checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                    activity!!.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
                ) {
                    //permission was not enabled
                    Log.d("PERMISSION_STATUS", "NOT GRANTED")
                    val permission = arrayOf(
                        android.Manifest.permission.CAMERA,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                    //show popup to request permission
                    requestPermissions(permission, PERMISSION_CODE_CAMERA)
                } else {
                    //permission already granted
                    Log.d("PERMISSION_STATUS", "YES GRANTED")
                    openCamera()
                }
            } else {
                //system os < Marshmallow
                openCamera()
            }
        }
        if (postion == 1){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (activity!!.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    //permission denied
                    val permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    requestPermissions(permissions, PERMISSION_CODE_GALLERY)
                } else {
                    //permission already granted
                    pickImageFromGallery()
                }
            } else {
                //System OS is < Marshmallow
                pickImageFromGallery()
            }
        }
    }


    fun changeImageCarnet(position: Int) {
        if (position == 0) {
            camera.setOnClickListener {
                openCamera()
            }
        } else {
            galeria.setOnClickListener {
                pickImageFromGallery()
            }
        }
    }



    //Inflate QuestionDialog
    private fun showQuestion(){
        val questionDialogPhoto = Question()
        questionDialogPhoto.setTargetFragment(this@CarnetFragment, 1)
        fragmentManager?.let { questionDialogPhoto.show(it, "") }
    }

    //Inflate RateDialog
    private fun showRating(){
        val ratingDialogItem = RateDialog()
        ratingDialogItem.setTargetFragment(this@CarnetFragment, 1)
        fragmentManager?.let { ratingDialogItem.show(it, "") }
    }
}

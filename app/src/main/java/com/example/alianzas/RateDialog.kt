package com.example.alianzas


import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment


class RateDialog : DialogFragment() {

    //Body Dialog
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(context!!)
            .setTitle(getString(R.string.dialog_title))
            .setView(R.layout.dialog_rate)
            .setPositiveButton(getString(R.string.dialog_ok)) { _, _ ->

            }
            .setNegativeButton(getString(R.string.dialog_cancel)) { _, _ ->

            }
            .create()
    }
}